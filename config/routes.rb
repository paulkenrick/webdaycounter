require 'resque/server'

Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'public/main#homepage'

  get 'login' => 'sessions#new', as: :login
  post 'login' => 'sessions#create', as: :create_login
  get 'logout' => 'sessions#destroy', as: :logout

  namespace :public do
    get 'homepage' => 'main#homepage', as: :homepage
    get 'signup' => 'signups#new', as: :signup
    post 'signup' => 'signups#create', as: :create_signup
  end

  namespace :account do
    get 'homepage' => 'main#homepage', as: :homepage
    resources :trackers do
      member do
        post 'dismiss_welcome_message' => 'trackers#dismiss_welcome_message', as: :dismiss_welcome_message
      end
    end
    resources :trips
  end

  mount Resque::Server.new, :at => "/resque"
end
