# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.destroy_all
Tracker.destroy_all
Trip.destroy_all

# Create tracker and trips for public show page
Tracker.create!(start_date: Date.new(2018, 1, 1), end_date: Date.new(2022, 12, 31))
Trip.create!(destination: "Spain", depart_home_date: Date.new(2018, 2, 3), return_home_date: Date.new(2018, 2, 12))
Trip.create!(destination: "Thailand", depart_home_date: Date.new(2018, 4, 21), return_home_date: Date.new(2018, 5, 14))
Trip.create!(destination: "Australia", depart_home_date: Date.new(2018, 6, 1), return_home_date: Date.new(2018, 6, 19))
Trip.create!(destination: "France", depart_home_date: Date.new(2018, 6, 27), return_home_date: Date.new(2018, 7, 2))
Trip.create!(destination: "Dubai", depart_home_date: Date.new(2018, 10, 12), return_home_date: Date.new(2018, 10, 30))
Trip.create!(destination: "Bulgaria", depart_home_date: Date.new(2018, 12, 16), return_home_date: Date.new(2019, 1, 2))

# Create tracker and trips for account show page
user = User.create(email: "test.user@example.com", password: "password", status: "approved")
Tracker.create!(user_id: user.id, start_date: Date.new(2019, 1, 1), end_date: Date.new(2021, 12, 31))
Trip.create!(user_id: user.id, destination: "New Zealand", depart_home_date: Date.new(2019, 3, 12), return_home_date: Date.new(2019, 3, 28))
Trip.create!(user_id: user.id, destination: "Japan", depart_home_date: Date.new(2019, 5, 4), return_home_date: Date.new(2019, 5, 17))
Trip.create!(user_id: user.id, destination: "Seychelles", depart_home_date: Date.new(2019, 6, 27), return_home_date: Date.new(2019, 7, 9))
Trip.create!(user_id: user.id, destination: "Germany", depart_home_date: Date.new(2019, 8, 18), return_home_date: Date.new(2019, 8, 25))
Trip.create!(user_id: user.id, destination: "Russia", depart_home_date: Date.new(2019, 10, 30), return_home_date: Date.new(2019, 11, 8))
Trip.create!(user_id: user.id, destination: "Mexico", depart_home_date: Date.new(2019, 12, 11), return_home_date: Date.new(2019, 12, 29))
