class AddShownWelcomeToTrackers < ActiveRecord::Migration[5.2]
  def change
    add_column :trackers, :shown_welcome, :boolean, default: false
  end
end
