class AddStatusToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :status, :string, default: 'approved'
    add_column :users, :auth_token, :string
    add_column :users, :last_login, :datetime

    add_timestamps :users
  end
end
