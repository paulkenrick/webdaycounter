class CreateTrackersAndTrips < ActiveRecord::Migration[5.2]
  def change
    create_table :trackers do |t|
      t.date :start_date
      t.date :end_date
    end

    create_table :trips do |t|
      t.date :depart_home_date
      t.date :return_home_date
      t.string :destination
    end
  end
end
