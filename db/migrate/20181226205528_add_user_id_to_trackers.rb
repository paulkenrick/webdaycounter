class AddUserIdToTrackers < ActiveRecord::Migration[5.2]
  def change
    add_column :trackers, :user_id, :integer
  end
end
