class SessionsController < ApplicationController

  layout 'empty'

  def create
    @user = User.find_by(email: params[:user][:email])

    if @user && @user.authenticate(params[:user][:password])
      if @user && @user.status == "initial"
        flash.now[:warning] = "Your account has not yet been activated.  Please check you emails."
      else
        session[:auth_token] = @user.auth_token
        log_user_in(@user)
        flash[:success] = "Successfully logged you in."
        render js: "window.location='#{account_homepage_path}'"
      end
    else
      @user = User.new(email: params[:user][:email])
    end
  end

  def destroy
    session.delete(:auth_token)
  	flash[:success] = "You have successfully logged out"
    redirect_to root_path
  end

end
