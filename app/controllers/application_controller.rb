class ApplicationController < ActionController::Base

  def authorise
    if current_user
      Rails.logger.info("Auth: Currently logged in as #{current_user.email}.")
    else
      flash[:info] = "Please log in."
      redirect_to login_path, info: "Please log in"
    end
  end

  def current_user
    @current_user ||= User.find_by(auth_token: session[:auth_token])
  end

  def log_user_in(user)
    session[:auth_token] = user.auth_token
    user.update_attributes(last_login: Time.now)
  end
end
