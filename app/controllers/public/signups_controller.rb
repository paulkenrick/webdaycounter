class Public::SignupsController < ApplicationController

  layout 'empty'

  def new
    @user = User.new
  end

  def create
    @user = User.new(signup_params)

    @user.build_tracker(start_date: Date.current.beginning_of_year, end_date: Date.current.end_of_year + 1.year)

    if @user.save
      session[:auth_token] = @user.auth_token
      log_user_in(@user)
      flash[:success] = "Successfully created account and logged you in."
      render js: "window.location='#{account_homepage_path}'"
    else
      @errors = { email: @user.errors[:email], password: @user.errors[:password], password_confirmation: @user.errors[:password_confirmation] }.to_json.html_safe
    end
  end

  def signup_params
    params.require(:user).permit(
      :email, :password, :password_confirmation
    )
  end
end
