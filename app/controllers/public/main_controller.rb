class Public::MainController < ApplicationController

  # layout 'empty'

  def homepage
    @user = User.new
    @tracker = Tracker.first
    @all_trips = Trip.where(user_id: nil).order(depart_home_date: :desc)
    @relevant_trips = @all_trips.relevant_for_dates(@tracker.start_date, @tracker.end_date)

    @trips_json = {}
    @relevant_trips.each do |trip|
      @trips_json[trip.id.to_s] = trip
    end
    @trips_json = @trips_json.to_json

    @absent_days_in_view = 0
    @relevant_trips.each do |trip|
      if trip.relevant_to_view(@tracker.start_date, @tracker.start_date + 1.year - 1.day)
        @absent_days_in_view += trip.absent_days_in_view(@tracker.start_date, @tracker.start_date + 1.year - 1.day)
      end
    end
  end
end
