class Account::TrackersController < ApplicationController

  def update
    @tracker = Tracker.find(params[:id])

    @tracker.assign_attributes(tracker_params)
    @tracker.assign_attributes(shown_welcome: true)
    if @tracker.save
      flash[:success] = "Successfully updated tracker dates."
      render js: "window.location='#{account_homepage_path}'"
    else
      @errors = { start_date: @tracker.errors[:start_date], end_date: @tracker.errors[:end_date], end_date_12_months_after_start_date: @tracker.errors[:end_date_12_months_after_start_date] }.to_json.html_safe
    end
  end

  def dismiss_welcome_message
    tracker = Tracker.find(params[:id])
    return if tracker.user != current_user

    tracker.update_attributes(shown_welcome: true)
  end

  private

  def tracker_params
    params.require(:tracker).permit(:start_date, :end_date)
  end

end
