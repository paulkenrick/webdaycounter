class Account::TripsController < ApplicationController

  def create
    @trip = Trip.new(trip_params)
    @trip.user_id = current_user.id

    if @trip.save
      flash[:success] = "Successfully created new trip."
      render js: "window.location='#{account_homepage_path}'"
    else
      @errors = { destination: @trip.errors[:destination], depart_home_date: @trip.errors[:depart_home_date], return_home_date: @trip.errors[:return_home_date], overlaps: @trip.errors[:overlaps], return_date_after_depart_date: @trip.errors[:return_date_after_depart_date] }.to_json.html_safe
    end
  end

  def update
    @trip = Trip.find(params[:id])

    @trip.assign_attributes(trip_params)
    if @trip.save
      flash[:success] = "Successfully updated trip."
      render js: "window.location='#{account_homepage_path}'"
    else
      @errors = { destination: @trip.errors[:destination], depart_home_date: @trip.errors[:depart_home_date], return_home_date: @trip.errors[:return_home_date], overlaps: @trip.errors[:overlaps], return_date_after_depart_date: @trip.errors[:return_date_after_depart_date] }.to_json.html_safe
    end
  end

  def destroy
    @trip = Trip.find(params[:id])
    @trip.destroy
    redirect_to account_homepage_path
  end

  private

  def trip_params
    params.require(:trip).permit(:destination, :depart_home_date, :return_home_date)
  end

end
