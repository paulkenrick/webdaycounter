class Account::MainController < ApplicationController
  before_action :authorise

  def homepage
    @tracker = current_user.tracker
    @all_trips = current_user.trips.order(depart_home_date: :desc)
    @relevant_trips = @all_trips.relevant_for_dates(@tracker.start_date, @tracker.end_date)

    @failed_periods = @tracker.failed_periods.reverse

    @trips_json = {}
    @relevant_trips.each do |trip|
      @trips_json[trip.id.to_s] = trip
    end
    @trips_json = @trips_json.to_json

    @absent_days_in_view = 0
    @relevant_trips.each do |trip|
      if trip.relevant_to_view(@tracker.start_date, @tracker.start_date + 1.year - 1.day)
        @absent_days_in_view += trip.absent_days_in_view(@tracker.start_date, @tracker.start_date + 1.year - 1.day)
      end
    end
  end
end
