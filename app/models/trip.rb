class Trip < ApplicationRecord
  include Destinations

  belongs_to :user, optional: true

  validates :destination, presence: { message: "Please provide a valid destination." }
  validates :depart_home_date, presence: { message: "Please select a valid date for departing the UK." }
  validates :return_home_date, presence: { message: "Please select a valid date for arrive back in the UK." }
  validate  :return_date_after_depart_date
  # validate  :no_overlaps

  scope :relevant_for_dates, -> (from, to) { where(
    "depart_home_date <= ? AND return_home_date >= ? OR
    depart_home_date >= ? AND return_home_date <= ? OR
    depart_home_date <= ? AND return_home_date >= ? OR
    depart_home_date <= ? AND return_home_date >= ?",
     from, from, from, to, to, to, from, to) }

  def day_count_for_display
    if self.return_home_date == self.depart_home_date
      return 1 # 0 absent days for the calculation, but display one day on the timeline so users know where the trip is
    else
      (self.return_home_date - self.depart_home_date).to_i + 1 # plus one to include both travel days for display purposes only
    end
  end

  def day_count
    if self.return_home_date == self.depart_home_date
      return 0 # travel days count as present, so if you department and return on same day, no absent days
    else
      (self.return_home_date - self.depart_home_date).to_i - 1 # minus one to remove the return date (travel days are not absent)
    end
  end

  def relevant_to_view(view_start_date, view_end_date)
    if self.return_home_date < view_start_date || self.depart_home_date > view_end_date
      return false
    end
    true
  end

  def absent_days_in_view(view_start_date, view_end_date)
    absent_days = 0

    current_date = self.depart_home_date + 1.day
    while(current_date < self.return_home_date)
      if current_date >= view_start_date && current_date <= view_end_date
        absent_days += 1
      end
      current_date += 1.day
    end
    absent_days
  end

  def return_date_after_depart_date
    if self.depart_home_date && self.return_home_date
      if self.return_home_date < self.depart_home_date
        self.errors.add(:return_date_after_depart_date, "Return date must be after departure date")
      end
    end
  end

  def no_overlaps
    if self.depart_home_date && self.return_home_date
      if self.user.nil?
        if Trip.where(user_id: nil).relevant_for_dates(self.depart_home_date + 1.day, self.return_home_date - 1).count > 0
          self.errors.add(:overlaps, 'Trip cannot overlap another trip.')
        end
      elsif self.user.trips.where.not(id: self.id).relevant_for_dates(self.depart_home_date + 1.day, self.return_home_date - 1).count > 0
        self.errors.add(:overlaps, 'Trip cannot overlap another trip.')
      end
    end
  end

end
