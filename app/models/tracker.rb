class Tracker < ApplicationRecord

  belongs_to :user, optional: true

  validates :start_date, presence: { message: "Please select a valid start date." }
  validates :end_date, presence: { message: "Please select a valid end date." }
  validate :end_date_12_months_after_start_date

  def day_count
    self.end_date - self.start_date + 1
  end

  def trips
    self.user.trips.relevant_for_dates(self.start_date, self.end_date)
  end


  def failed_periods
    trips = self.trips
    first_end_date = self.end_date
    last_start_date = self.start_date

    current_start_date = first_end_date - 12.months + 1.day
    current_end_date = first_end_date

    failed_views = []
    while current_start_date >= last_start_date
      trips_in_current_view = trips.relevant_for_dates(current_start_date, current_end_date)

      absent_days_in_current_view = 0
      trips_in_current_view.each do |trip|
        absent_days_in_current_view += trip.absent_days_in_view(current_start_date, current_end_date)
      end

      if absent_days_in_current_view > 180
        failed_views << { start_date: current_start_date, end_date: current_end_date, absent_days: absent_days_in_current_view }
      end

      current_end_date -= 1.day
      current_start_date = current_end_date - 12.months + 1.day
    end

    failed_views
  end

  private

  def end_date_12_months_after_start_date
    if self.start_date && self.end_date
      if self.start_date + 12.months - 1 > self.end_date
        self.errors.add(:end_date_12_months_after_start_date, "End date must be at least 12 months after start date")
      end
    end
  end

end
