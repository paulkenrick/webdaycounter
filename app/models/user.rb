class User < ApplicationRecord
  
  has_secure_password validations: false
  has_secure_token :auth_token

  has_one :tracker
  has_many :trips

  validates :email, presence: { message: "Please include a valid email address"}
  validates :password, length: { minimum: 6, message: "Please include a password of 6 or more characters" }, confirmation: { message: "Please ensure confirmation matches password"}

end
