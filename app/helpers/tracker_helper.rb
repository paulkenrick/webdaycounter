module TrackerHelper

  def scrollbar_width(tracker)
    multiplyer = tracker.day_count.to_f / 365
    (multiplyer * red_window_percentage_width) + (red_window_percentage_margins * 2)
  end

  def red_window_percentage_width
    80
  end

  def red_window_percentage_margins
    (100 - red_window_percentage_width) / 2
  end

  def timeline_percentage_margins(tracker)
    red_window_percentage_margins / (scrollbar_width(tracker) / 100)
  end

end
