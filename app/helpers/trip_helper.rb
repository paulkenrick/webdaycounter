module TripHelper

  def get_trip_position(trip, tracker)
    trip_effective_depart_home_date = trip.depart_home_date < tracker.start_date ? tracker.start_date : trip.depart_home_date

    position_in_days = (trip_effective_depart_home_date - tracker.start_date).to_i
    position_in_percentage = (position_in_days.to_f / tracker.day_count.to_f) * 100
    position_in_percentage
  end

  def get_trip_width(trip, tracker)
    trip_effective_day_count_for_display = nil
    if trip.depart_home_date < tracker.start_date
      trip_effective_day_count_for_display = trip.day_count_for_display - (tracker.start_date - trip.depart_home_date)
    elsif trip.return_home_date > tracker.end_date
      trip_effective_day_count_for_display = trip.day_count_for_display - (trip.return_home_date - tracker.end_date)
    else
      trip_effective_day_count_for_display = trip.day_count_for_display
    end

    width_in_percentage = (trip_effective_day_count_for_display.to_f / tracker.day_count.to_f) * 100
    width_in_percentage
  end

  def get_country_label_position(trip, tracker)
    trip_effective_depart_home_date = trip.depart_home_date < tracker.start_date ? tracker.start_date : trip.depart_home_date

    position_in_days = (trip_effective_depart_home_date - tracker.start_date).to_i
    position_in_percentage = (position_in_days.to_f / tracker.day_count.to_f) * 100
    position_in_percentage
  end

  def get_country_label_width(trip, tracker)
    trip_effective_day_count_for_display = nil
    if trip.depart_home_date < tracker.start_date
      trip_effective_day_count_for_display = trip.day_count_for_display - (tracker.start_date - trip.depart_home_date)
    elsif trip.return_home_date > tracker.end_date
      trip_effective_day_count_for_display = trip.day_count_for_display - (trip.return_home_date - tracker.end_date)
    else
      trip_effective_day_count_for_display = trip.day_count_for_display
    end

    width_in_percentage = (trip_effective_day_count_for_display.to_f / tracker.day_count.to_f) * 100
    width_in_percentage
  end

end
