$(document).ready(function(){

  // initialise any datepickers on the page
  $('.datepicker').datepicker({
    'format': 'dd/mm/yyyy',
    autoclose: true
  });

});
