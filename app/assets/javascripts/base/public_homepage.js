$(document).ready(function(){
  if ($('.public-homepage').length > 0) {
    $('#edit-tracker-button, #new-trip-button, .edit-trip-button, .delete-trip-button').on('click', function(){
      $('.error-message').remove();
      $('.force-login-message').remove();
      $('.modal-body').prepend("<p class='force-login-message text-center alert alert-warning'>Log into your account in order to save your trips to the system and view your own day counter.</p>");
    });

    $('.login-button').on('click', function(){
      $('#signup-form-modal').modal('hide')
      $('.error-message').remove();
      $('.force-login-message').remove();
    });

    $('.signup-button').on('click', function(){
      $('#login-form-modal').modal('hide')
      $('.error-message').remove();
      $('.force-login-message').remove();
      $('.is-invalid').removeClass('is-invalid')
      $('.invalid-feedback').remove()
    });
  }
});
