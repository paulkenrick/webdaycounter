$(document).ready(function(){
  if ($('.account-homepage').length > 0) {

    // Set welcome modal to popup after 2 seconds
    setTimeout(function() {
      $('[id^="welcome-modal-"]').modal();
    }, 2000);

    // Permanently dismiss the welcome message popup
    $('#dismiss-welcome-button').on('click', function(e){
      trackerID = $(e.currentTarget).parents('.modal').attr('id').split('-')[2];
      $.ajax({
        method: 'post',
        url: '/account/trackers/' + trackerID + '/dismiss_welcome_message'
      });
    });

    // Remove errors from the edit tracker modal when it's opened (and ensure dates are shown)
    $('#edit-tracker-link').on('click', function(){
      // Remove errors when reloading form.
      var errorMessage = $('#edit-tracker-dates-modal').find('.error-message')
      $(errorMessage).remove();

      var trackerStartDateField = $('#tracker_start_date');
      $(trackerStartDateField).removeClass('is-invalid');
      $(trackerStartDateField).parents('.form-group').find('.invalid-feedback').remove();

      var trackerEndDateField = $('#tracker_end_date');
      $(trackerEndDateField).removeClass('is-invalid');
      $(trackerEndDateField).parents('.form-group').find('.invalid-feedback').remove();

      // Replace dates when reloading form if they were removed
      var dateVar = $('.start-date-label').html();
      var dsplit = dateVar.split("/");
      var date = new Date(dsplit[2],dsplit[1]-1,dsplit[0]);
      $(trackerStartDateField).datepicker("setDate", date)

      var dateVar = $('.end-date-label').html();
      var dsplit = dateVar.split("/");
      var date = new Date(dsplit[2],dsplit[1]-1,dsplit[0]);
      $(trackerEndDateField).datepicker("setDate", date)
    });

    // Remove errors from the new trip modal when it's opened
    $('#new-trip-link').on('click', function(){
      var errorMessage = $('.new_trip').find('.error-message')
      $(errorMessage).remove();

      var tripDestinationField = $('.new_trip').find('#trip_destination');
      $(tripDestinationField).removeClass('is-invalid');
      $(tripDestinationField).parents('.form-group').find('.invalid-feedback').remove();

      var tripDepartHomeDateField = $('.new_trip').find('#trip_depart_home_date');
      $(tripDepartHomeDateField).removeClass('is-invalid');
      $(tripDepartHomeDateField).parents('.form-group').find('.invalid-feedback').remove();

      var tripReturnHomeDateField = $('.new_trip').find('#trip_return_home_date');
      $(tripReturnHomeDateField).removeClass('is-invalid');
      $(tripReturnHomeDateField).parents('.form-group').find('.invalid-feedback').remove();
    });

    // Remove errors from the edit trip modal when it's opened (and ensure dates are shown)
    $('[id^="edit-trip-link-').on('click', function(e){
      tripID = $(e.currentTarget).attr('id');
      modalID = 'edit-trip-' + tripID.split('-')[3] + '-modal';

      // Remove errors when reloading form.
      var errorMessage = $('#' + modalID).find('.error-message')
      $(errorMessage).remove();

      var tripDestinationField = $('#' + modalID).find('#trip_destination');
      $(tripDestinationField).removeClass('is-invalid');
      $(tripDestinationField).parents('.form-group').find('.invalid-feedback').remove();

      var tripDepartHomeDateField = $('#' + modalID).find('#trip_depart_home_date');
      $(tripDepartHomeDateField).removeClass('is-invalid');
      $(tripDepartHomeDateField).parents('.form-group').find('.invalid-feedback').remove();

      var tripReturnHomeDateField = $('#' + modalID).find('#trip_return_home_date');
      $(tripReturnHomeDateField).removeClass('is-invalid');
      $(tripReturnHomeDateField).parents('.form-group').find('.invalid-feedback').remove();

      // Replace dates when reloading form if they were removed
      var destination = $(e.currentTarget).parents('tr').find('.destination-cell').html()
      $(tripDestinationField).val(destination)

      var dateVar = $(e.currentTarget).parents('tr').find('.depart-home-date-cell').html()
      date = new Date(dateVar)
      $(tripDepartHomeDateField).datepicker("setDate", date)

      var dateVar = $(e.currentTarget).parents('tr').find('.return-home-date-cell').html()
      date = new Date(dateVar)
      $(tripReturnHomeDateField).datepicker("setDate", date)
    });

    // When changing trip's depart_home_date, update the return_home_date to match, if it's otherwise empty
    $('.depart_home_date').on('change', function(e){
      var returnHomeField = $(e.target).parents('.modal').find('.return_home_date')
      if ($(returnHomeField).val() == ""){
        var departureDateComponents = $(e.target).val().split('/')
        departureDate = new Date(departureDateComponents[2], departureDateComponents[1] - 1, departureDateComponents[0])
        nextDay = new Date((departureDate).valueOf() + 1000*3600*24);
        $(returnHomeField).datepicker('setDate', nextDay)
      }
    })
  }
});
